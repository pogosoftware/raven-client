using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RavenClient.Interfaces;
using RavenClient.Models;

namespace RavenClient
{
    public static class RavenClientServicesExtensions
    {
        public static void AddRavenClient(this IServiceCollection services, IConfiguration configuration, string sectionName)
        {
            services.Configure<RavenClientSettings>(configuration.GetSection(sectionName));
            services.AddSingleton<IRavenClient, Implementation.RavenClient>();
        }
    }
}