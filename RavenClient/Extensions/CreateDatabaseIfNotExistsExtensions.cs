using Raven.Client.Documents;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;

namespace RavenClient.Extensions
{
    public static class CreateDatabaseIfNotExistsExtensions
    {
        public static void CreateDatabaseIfNotExists(this IDocumentStore store, string databaseName)
        {
            var dbRecord = store.Maintenance.Server.Send(new GetDatabaseRecordOperation(databaseName));
            if (dbRecord == null)
            {
                store.Maintenance.Server.Send(new CreateDatabaseOperation(new DatabaseRecord(databaseName)));
            }
        }
    }
}