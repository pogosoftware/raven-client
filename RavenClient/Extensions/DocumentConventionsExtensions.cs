using System;
using System.Linq.Expressions;
using System.Reflection;
using Raven.Client.Documents.Conventions;

namespace RavenClient.Extensions
{
    public static class DocumentConventionsExtensions
    {
        public static void SetPropertyValue<TValue>(this DocumentConventions target, Expression<Func<DocumentConventions, TValue>> memberLamda, TValue value)
        {
            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(target, value, null);
                }
            }
        }
    }
}